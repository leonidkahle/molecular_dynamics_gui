#!/usr/bin/env python

import sys, os
import numpy as np
import Tkinter as Tk
from ttk import Frame
import matplotlib
import mpl_toolkits.mplot3d.axes3d as p3
from matplotlib import animation, gridspec
from matplotlib import pyplot as plt
from matplotlib import colors
from colorsys import rgb_to_hls, hls_to_rgb
#~ from ase.data.colors import cpk_colors as  jmol_colors
from ase.data.colors import jmol_colors
from ase.data import atomic_numbers, covalent_radii
from ase.io import read
from ase.calculators.emt import EMT
#~ from asap3 import EMT

from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.md.verlet import VelocityVerlet
from ase.md.langevin import Langevin
from ase import units

from time import sleep
import threading
matplotlib.use('TkAgg')

#~ from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.backend_bases import key_press_handler
from argparse import ArgumentParser


ps = ArgumentParser(argument_default=False, description='This is an MD-engine and visualizer. A molecular dynamics is run on the fly with a GUI')


ps.add_argument('--small', help='For smaller windows', action='store_true' )
passed_args = ps.parse_args(sys.argv[1:])


cmap = plt.cm.BuPu
#~ The atoms that the EMT calculator can handle:

    #~ 'Al': (-3.28, 3.00, 1.493, 1.240,  2.000,  1.169,  0.00700),
    #~ 'Cu': (-3.51, 2.67, 2.476, 1.652,  2.740,  1.906,  0.00910),
    #~ 'Ag': (-2.96, 3.01, 2.132, 1.652,  2.790,  1.892,  0.00547),
    #~ 'Au': (-3.80, 3.00, 2.321, 1.674,  2.873,  2.182,  0.00703),
    #~ 'Ni': (-4.44, 2.60, 3.673, 1.669,  2.757,  1.948,  0.01030),
    #~ 'Pd': (-3.90, 2.87, 2.773, 1.818,  3.107,  2.155,  0.00688),
    #~ 'Pt': (-5.85, 2.90, 4.067, 1.812,  3.145,  2.192,  0.00802),
    #~ # extra parameters - just for fun ...
    #~ 'H':  (-3.21, 1.31, 0.132, 2.652,  2.790,  3.892,  0.00547),
    #~ 'C':  (-3.50, 1.81, 0.332, 1.652,  2.790,  1.892,  0.01322),
    #~ 'N':  (-5.10, 1.88, 0.132, 1.652,  2.790,  1.892,  0.01222),
    #~ 'O':  (-4.60, 1.95, 0.332, 1.652,  2.790,  1.892,  0.00850)}

def collapse_into_unit_cell(point, cell):
    """Applies linear transformation to coordinate system based on crystal lattice, vectors. The inverse
    of that inverse transformation matrix with the point given results in the point being given as a multiples of lattice vectors
    Than take the integer of the rows to find how many times you have to shift the point back"""
    invcell = np.matrix(cell).T.I
    points_in_crystal = np.dot(invcell,point).tolist()[0] # point in crystal coordinates
    points_in_unit_cell = [i%1 for i in points_in_crystal] #point collapsed into unit cell
    return np.dot(cell.T, points_in_unit_cell).tolist()

def get_stat_quants(a):
    """Function to print the potential, kinetic and total energy"""
    epot = a.get_potential_energy() / len(a)
    ekin = a.get_kinetic_energy() / len(a)
    return epot, ekin, ekin / (1.5 * units.kB), epot + ekin
    
class GUIClass():
    
    def __init__(self, **kwargs):
        small_window = kwargs['small']
        def new_structure(val):
            try:
                sender = val.widget
                idx = sender.curselection()
                self.structure = sender.get(idx)   
            except:
                pass
            plt.clf()
            nx = xscale.get()
            ny = yscale.get()
            nz = zscale.get()
            strucname.set(self.structure)
            self.canvas.figure = self.initialize_atoms(self.structure,fig,gs1,gs2, repeat = [nx,ny,nz])
            self.canvas.draw()
            self.timestep = 0
            make_title()
            
        def start_animation():
            make_title()
            self.initialize_md()
            self.timestep_scale.config(state='disabled')
            self.temperature_scale.config(state='disabled')
            xscale.config(state='disabled')
            yscale.config(state='disabled')
            zscale.config(state='disabled')
            quit_button.config(state='disabled')
            start_button.config(state='disabled')
            listbox.config(state='disabled')
            stop_button.config(state='active')
            NVT_button.config(state = 'disabled')
            def worker():
                while True:
                    self.timestep += 1
                    self.get_new_timestep() #self.atoms, self.balls, canvas)
                    #~ sleep(1/speed.get())
                    if self.kill:
                        break
                self.timestep_scale.config(state='active')
                self.temperature_scale.config(state='active')
                xscale.config(state='active')
                yscale.config(state='active')
                zscale.config(state='active')
                quit_button.config(state='active')
                start_button.config(state='active')
                stop_button.config(state='disabled')
                NVT_button.config(state = 'active')
                listbox.config(state='normal')
            self.kill = False
            self.thread  =  threading.Thread(target = worker)
            self.thread.start()
        
        def make_title():
            title_str.set('{} at {} K'.format(self.structure, self.temperature_scale.get()))
            
        def stop_animation():
            self.kill = True


        def _quit():
            root.quit()     # stops mainloop
            root.destroy()  # this is necessary on Windows to prevent
                            # Fatal Python Error: PyEval_RestoreThread: NULL tstat


        
        
        #INITIALIZE canvas and Tk  Frame
        root = Tk.Tk()
        #~ root.configure(background='white')
        structure_list  = sorted([i.replace('.cif','') for i in os.listdir('./cifs/')])
        self.structure = 'Cu3Pd1' # structure_list[0] 
        root.wm_title("Molecular Dynamics")
        if small_window: fig = plt.figure(figsize = (14,8))
        else: fig = plt.figure(figsize = (20,12))
        self.canvas = FigureCanvasTkAgg(fig, master=root)
        
        


        
        #THE VARIABLES and BUTTONS FOR STRUCTURES:
        strucname = Tk.StringVar()
        if small_window: listbox = Tk.Listbox(master=root, height = 40, width =10)
        else: listbox = Tk.Listbox(master=root, height = 60, width =12)
        listbox.bind("<<ListboxSelect>>", new_structure)
        for cif in structure_list:
            listbox.insert('end', cif)
        xscale = Tk.Scale(master = root, from_=1, to=3,orient='horizontal', label = 'Nx', command = new_structure)
        yscale = Tk.Scale(master = root, from_=1, to=3,orient='horizontal', label = 'Ny', command = new_structure)
        zscale = Tk.Scale(master = root, from_=1, to=3,orient='horizontal', label = 'Nz', command = new_structure)
        
        self.timestep = 0

        # THE BUTTONS FOR SIMULATION:
        
        #speed settings
        speed = Tk.Scale(master = root, from_=1, to=100,orient='horizontal', label = 'speed')
        speed.set(30)
        
        #collapse
        self.collapse = Tk.IntVar()
        collapse_button = Tk.Checkbutton(root, text="Collapse", variable=self.collapse)
        
        #leave trails
        self.leave_trails = Tk.IntVar()
        self.leave_trails.set(1)
        trails_button = Tk.Checkbutton(root, text="Leave trails", variable=self.leave_trails)
                
        #print level:
        
        self.print_level = Tk.Scale(root,from_=1, to=100,orient='horizontal', label = 'Print level')
        self.print_level . set (1)

        #timestep
        #~ self.timestep = Tk.DoubleVar()
        #~ self.timestep.set(1.0)
        #~ timestep_field = Tk.Entry(root, textvariable =self.timestep)
        self.timestep_scale = Tk.Scale(root,from_=1, to=10,orient='horizontal', label = 'Timestep [fs]')
        self.timestep_scale . set (5)
        
        #temperature
        self.temperature_scale = Tk.Scale(root, from_=1, to=3000,orient='horizontal', label = 'Temperature [K]')
        self.temperature_scale.set(600)
        
        # ensemble:
        self.constant_temperature = Tk.IntVar()
        self.constant_temperature.set(0)
        NVT_button = Tk.Checkbutton(root, text="Const. Temp.", variable=self.constant_temperature)
        
        
        #make the title:
        title_str = Tk.StringVar()
        make_title()
        title_label = Tk.Label(root, textvariable=title_str, font=("Helvetica", 16))
        
        
        #MD CONTROL:
        start_button = Tk.Button(master=root, text='START', command=start_animation)
        stop_button = Tk.Button(master=root, text='STOP', command=stop_animation)
        stop_button.config(state='disabled')
        quit_button = Tk.Button(master=root, text='QUIT', command=_quit)
        
        
        
        ### Ok, GEOMETRY: 
        title_label.grid(row = 1, column = 1, columnspan = 3)
        listbox.grid(row = 2, column = 1, rowspan = 12)
        self.canvas._tkcanvas.grid(row = 2, column = 2, rowspan = 12)
        
        
        start_button.grid(row=2, column=3)
        stop_button.grid(row=3, column=3)
        quit_button.grid(row=4, column=3)
        collapse_button.grid(row = 5, column = 3, sticky='w')
        trails_button.grid(row = 6, column = 3, sticky='w')
        speed.grid(row = 7, column=3)
        self.print_level.grid(row =8, column =3)
       

        xscale.grid(row = 9, column=3)
        yscale.grid(row = 10, column=3)
        zscale.grid(row = 11, column=3)
        
        
        self.timestep_scale.grid(row = 12, column = 3)
        self.temperature_scale.grid(row = 13,column = 3)
        
        NVT_button.grid(row =14, column =  3)

        #Ok, start:
        if small_window:    
            gs1 = gridspec.GridSpec(2,1, height_ratios = [3,1], left = 0, right = 1, top = 1, bottom = 0.1, hspace=0.01 )
            gs2 = gridspec.GridSpec(2,1, height_ratios = [3,1], left = 0.05, right = 0.95, top = 1, bottom = 0.07, hspace=0.05 )
         
            
        else:    
            gs1 = gridspec.GridSpec(2,1, height_ratios = [3,1], left = 0, right = 1, top = 1, bottom = 0.04, hspace=0.02 )
            gs2 = gridspec.GridSpec(2,1, height_ratios = [3,1], left = 0.05, right = 0.95, top = 1, bottom = 0.05, hspace=0.06 )
        
        self.initialize_atoms(self.structure , fig, gs1, gs2,repeat = [1,1,1])
        Tk.mainloop()
    
    def initialize_atoms(self, filename, fig, gs1, gs2, repeat = [1,1,1]):
        
        #~ fig.set_facecolor('black')
        #~ ax = p3.Axes3D(fig, axisbg='black')
        
        ax = fig.add_subplot(gs1[0], projection='3d', axisbg='black')
        ax.set_axis_off()
        ax.axis('equal')
        if not filename.endswith('.cif'):
            filename += '.cif'
        atoms = read('cifs/'+filename)
        atoms = atoms.repeat(repeat)
        pp = [[i,j,k] for i in range(2) for j in range(2) for k in range(2)]
        #~ print atoms.cell
        for p1 in pp:
            for p2 in pp:
                #skip unnecessary points:
                counts = [p1[i] == p2[i] for i in range(3)]
                if not counts.count(True) == 2:
                    continue
                #~ point  = [[i*vec_component for vec_component in vector] for i, vector in zip(
                #~ print np.dot([p1], atoms.cell).tolist()[0], np.dot([p2], atoms.cell).tolist()[0]
                ax.plot(*zip(np.dot([p1], atoms.cell).tolist()[0], np.dot([p2], atoms.cell).tolist()[0]), color = 'yellow')
        
        X, Y, Z = [np.array(i) for i in zip(*atoms.positions)]
        max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max() / 2.0

        mean_x = X.mean()
        mean_y = Y.mean()
        mean_z = Z.mean()
        ax.set_xlim(mean_x - max_range, mean_x + max_range)
        ax.set_ylim(mean_y - max_range, mean_y + max_range)
        ax.set_zlim(mean_z - max_range, mean_z + max_range)
        ax.set_xlim(mean_x - max_range, mean_x + max_range)
        ax.set_ylim(mean_y - max_range, mean_y + max_range)
        ax.set_zlim(mean_z - max_range, mean_z + max_range)
        
        
        atoms_simulation = []
        track_lines = []
        for indeks, pos in enumerate(atoms.positions):
            ele = atoms[indeks].symbol
            color = jmol_colors[atomic_numbers[ele]]
            size =  50*covalent_radii[atomic_numbers[ele]]
            atoms_simulation.append(ax.plot(*zip(pos), marker='o', # linestyle = 'points', \
                    color=color,markersize=size, alpha =0.95)[0])
            pos_hack = [[i] for i in pos]
            ax.plot(*pos_hack,  #linestyle = 'points', 
                    marker='.', markersize=10, color=color)


        #Do the legend:
        
        atoms_set_list = list(set([atom.symbol for atom in atoms]))
        handles = []
        for ele in atoms_set_list:
            color = jmol_colors[atomic_numbers[ele]]
            handles.append(ax.plot([], [], [], marker='o', #linestyle = 'points', 
                    color=color, markersize = 10)[0])
        
        ax.legend(handles, atoms_set_list, loc='upper left', numpoints=1, scatterpoints=1)
        
        
        
        ax.autoscale(False)
        self.ax = ax
        self.atoms = atoms
        self.balls = atoms_simulation
        
        
        
        
        
        fig.subplots_adjust(top=1, bottom = 0)
        
        ax2 = fig.add_subplot(gs2[1])
        plt.ylabel('Energy [eV / atom]')
        plt.xlabel('Time [fs]')
        self.ax2 = ax2
        ax3 = ax2.twinx() #fig.add_subplot(gs2[2])
        xticks = ax3.yaxis.get_major_ticks()
        xticks[-1].label1.set_visible(False)
        plt.ylabel('Temperature [K]')
        self.ax3 = ax3
        
        
        self.atoms.set_calculator(EMT())
        
        epot, ekin, temp, etot = get_stat_quants(self.atoms)
        
        l1 = self.ax2.scatter(self.timestep, epot, color = 'r', label = 'pot')
        l2 =self.ax2.scatter(self.timestep, ekin, color = 'b')
        
        l3 = self.ax2.scatter(self.timestep, etot, color = "#EC00B4", label = 'Etot')
        l4 = self.ax3.scatter(self.timestep, temp, color = "#110E10")
        ax2.legend([l1, l2, l3, l4], ['EPot', 'Ekin', 'Etot', 'Temp'], loc = 'upper left')
        return fig
        
    def initialize_md(self):
        
        # Set the momenta corresponding to set temperature
        MaxwellBoltzmannDistribution(self.atoms, self.temperature_scale.get() * units.kB)

        # We want to run MD with constant energy using the VelocityVerlet algorithm.

        if self.constant_temperature.get():
            dyn = Langevin(self.atoms, self.timestep_scale.get() * units.fs, self.temperature_scale.get() * units.kB, 0.002)

        else:
            dyn = VelocityVerlet(self.atoms, self.timestep_scale.get() * units.fs)  # 5 fs time step.


        self.dyn = dyn
        self.trajectory = []
        

    
    
    def get_new_timestep(self):
        self.dyn.run(1)
        self.trajectory.append(self.atoms.positions)
        for index, ball in enumerate(self.balls):
            #~ newdata = [[i] for i in collapse_into_unit_cell(timestep[indeks], np.array(self.cell))]
            #~ print self.collapse
            if self.collapse.get():
                newpos = collapse_into_unit_cell(self.atoms.positions[index], self.atoms.cell)
            else:
                newpos = self.atoms.positions[index]
            newdata = [[i] for i in newpos]
            ball.set_data(*newdata[:2])
            ball.set_3d_properties(newdata[2])
            if self.leave_trails.get():
                self.ax.plot(*newdata, linestyle = 'points', marker = '.', markersize = 5, color = ball.get_color())
        
        if not self.timestep % self.print_level.get():
            ts = self.timestep_scale.get() * self.timestep
            epot, ekin, temp, etot = get_stat_quants(self.atoms)            
            self.ax2.scatter(ts, epot, color = 'r')
            self.ax2.scatter(ts, ekin, color = 'b')
            self.ax2.scatter(ts, etot, color = "#EC00B4")
            self.ax3.scatter(ts, temp, color = "#110E10")
        
        self.canvas.draw()

gc = GUIClass(**passed_args.__dict__)




# If you put root.destroy() here, it will cause an error if
# the window is closed with the window manager.


